# GitLab Duo Coffee Chat: C++, SQlite and CMake refactoring adventures with Chat and Code Suggestions 

Host: @dnsmichi 
Guest: @iganbaruch 

## Recording

[![GitLab Duo Coffee Chat](https://img.youtube.com/vi/zGOo1jzQ5zM/0.jpg)](https://youtu.be/zGOo1jzQ5zM)

## Summary

Starting development on a backend service in C++ with user management in a database ... what could possibly go wrong? 

This is a different learning session, with many things failing and the goal to fix them. You will see thinking aloud, debugging strategies, and a final success. 

 CMake configuration went fine until it could not find the SQLite include/library files. Debugging linker errors is not fun. The goal here was to use GitLab Duo Chat for all question and only fallback to a browser search tor fetch current CMake configuration insights. 

Code Generation prompts successfully generated code for PostgreSQL and SQLite. Decided to move the PoC forward with SQLite because of fewer dependencies. 

The goal was to test the /refactor code task with different refined instructions to improve error handling, functions, etc. -- and therefore repeat SQL instructions and database operations as much as possible, to actually write lousy code and improve it later.

Refactoring the code includes splitting the functions into a separate source code file and preparing the structure for additional unit tests, which we will generate in a future coffee chat, using /tests in Duo Chat. 

Key takeaways: 

- You can ask GitLab Duo Chat anything - from CMake configuration to different methods to print variables, check file locations, etc. and debug why SQLite libraries are not found
- /refactor with additional instructions helps specific development style guide requirements. Works very well with C++. 
- Throughout the session, 33+ questions were asked in Duo Chat, including code tasks and prompt refinements to get better help. Little interaction was required with other knowledge sources, allowing focus on the actual problems. 


## Resources

- YouTube Playlist: https://go.gitlab.com/xReaA1 
- GitLab Duo: https://go.gitlab.com/Z1vBGD 
- Documentation: https://go.gitlab.com/rSbrTI 
    - GitLab Duo Code Suggestions: https://go.gitlab.com/XIuZ5q 
    - GitLab Duo Chat: https://go.gitlab.com/XLpq9a 
- Talk: Efficient DevSecOps Workflows with a little help from AI: https://go.gitlab.com/T864XF 
- Code Suggestions feedback issue: https://go.gitlab.com/07r1sv 
- Duo Chat feedback issue: https://go.gitlab.com/XTU14S 


## Usage

### Requirements

On macOS, install the developer command line tools which provide Apple Clang as compiler. Additionally, the following Homebrew package need to be installed:

```
brew install sqlite3 cmake 
```

@dnsmichi 's Macbook setup is documented in https://gitlab.com/dnsmichi/dotfiles 

### Build 

```shell
mkdir build
cd build
cmake ..
cd ..

make -C build 
```

### Run 

```shell
cd build

rm *.db 
./gitlab-duo
```

## License 

[cmake/FindSQLite3.cmake](cmake/FindSQLite3.cmake) was imported from https://github.com/valhalla/valhalla/blob/007042ac414988209a759f342de923cf19a4f02b/cmake/FindSQLite3.cmake original copyright and license are due.

Everything else is licensed under the MIT license.

