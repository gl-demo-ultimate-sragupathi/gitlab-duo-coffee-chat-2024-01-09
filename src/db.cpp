#include "../include/db.h"
#include <iostream>

int createUserTable(sqlite3* db) {
  sqlite3_stmt* stmt;

  std::string sql = "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, name TEXT, email TEXT)";

  int rc = sqlite3_prepare_v2(db, sql.c_str(), sql.size(), &stmt, NULL);

  if (rc != SQLITE_OK) {
    std::cout << "Error creating table: " << sqlite3_errmsg(db) << std::endl;
    return rc;
  }
  
  rc = sqlite3_step(stmt);
  sqlite3_finalize(stmt);
  
  return rc;
}

int insertUser(sqlite3* db, const User& user) {
  sqlite3_stmt* stmt;
  std::string sql = "INSERT INTO users (id, name, email) VALUES (?, ?, ?)";
  
  int rc = sqlite3_prepare_v2(db, sql.c_str(), sql.size(), &stmt, NULL);
  if (rc != SQLITE_OK) {
    std::cout << "Error preparing insert: " << sqlite3_errmsg(db) << std::endl;
    return rc; 
  }

  sqlite3_bind_int(stmt, 1, user.id);
  sqlite3_bind_text(stmt, 2, user.name.c_str(), user.name.size(), SQLITE_TRANSIENT);
  sqlite3_bind_text(stmt, 3, user.email.c_str(), user.email.size(), SQLITE_TRANSIENT);

  rc = sqlite3_step(stmt);
  sqlite3_reset(stmt);
  sqlite3_finalize(stmt);

  return rc;
}

int printUsers(sqlite3* db) {
  sqlite3_stmt* stmt;
  std::string sql = "SELECT * FROM users";
  int rc = sqlite3_prepare_v2(db, sql.c_str(), sql.size(), &stmt, NULL);
  if (rc != SQLITE_OK) {
    std::cout << "Error preparing select: " << sqlite3_errmsg(db) << std::endl;
    return rc;
  }
  
  while (sqlite3_step(stmt) == SQLITE_ROW) {
    // print user 
    int id = sqlite3_column_int(stmt, 0);
    std::string name = (const char*)sqlite3_column_text(stmt, 1);
    std::string email = (const char*)sqlite3_column_text(stmt, 2);
    
    std::cout << "User: " << id << ", " << name << ", " << email << std::endl;

  }

  sqlite3_finalize(stmt);
  return SQLITE_OK; 
}
