// Create a backend service that stores data in SQLite
// Implement a user table in the main function
// Insert user test data 
// Use best practice libraries and C++17 as standard
// Include all necessary headers

#include "../include/db.h"
#include <iostream>



int main() {

  sqlite3* db;
  sqlite3_open("database.db", &db);

  // TODO: Evaluate a drop all function for testing 

// TODO remove after refactoring 
  sqlite3_stmt* stmt;
  std::string sql;

  if (createUserTable(db) == SQLITE_DONE) {
    std::cout << "Table created successfully" << std::endl;
  } else {
    std::cout << "Error creating table" << std::endl;
  }

  User user1 = {1, "John Doe", "john@example.com"};
  User user2 = {2, "Jane Doe", "jane@example.com"};

  if (insertUser(db, user1) == SQLITE_DONE) {
    std::cout << "User " << user1.name << " inserted successfully" << std::endl;
  } else {
    std::cout << "Error inserting user " << user1.name << std::endl;
  }

  if (insertUser(db, user2) == SQLITE_DONE) {
    std::cout << "User " << user2.name << "inserted successfully" << std::endl;
  } else {
    std::cout << "Error inserting user " << user2.name << std::endl;
  }

  // Print users 
  printUsers(db);

  // TODO Michi, 2024-01-09: CONTINUE refactoring next week


  // Delete one user, and update the other user to use a gitlab.com email address
  sql = "DELETE FROM users WHERE id = ?";
  sqlite3_prepare_v2(db, sql.c_str(), sql.size(), &stmt, NULL);
  sqlite3_bind_int(stmt, 1, user1.id);
  sqlite3_step(stmt);
  sqlite3_reset(stmt);

  sql = "UPDATE users SET email = ? WHERE id = ?";
  sqlite3_prepare_v2(db, sql.c_str(), sql.size(), &stmt, NULL);
  sqlite3_bind_text(stmt, 1, "jane@gitlab.com", -1, SQLITE_TRANSIENT);
  sqlite3_bind_int(stmt, 2, user2.id);
  sqlite3_step(stmt);

  sqlite3_finalize(stmt);
  sqlite3_close(db);

  // Read the data from the database
  sqlite3_open("database.db", &db);

  // TODO error handling

  sql = "SELECT * FROM users";
  sqlite3_prepare_v2(db, sql.c_str(), sql.size(), &stmt, NULL);

  while (sqlite3_step(stmt) == SQLITE_ROW) {
    int id = sqlite3_column_int(stmt, 0);
    const unsigned char* name = sqlite3_column_text(stmt, 1);
    const unsigned char* email = sqlite3_column_text(stmt, 2);
    std::cout << id << " " << name << " " << email << std::endl;
  } 

  sqlite3_finalize(stmt);
  sqlite3_close(db);

  // Add a new database column for storing a hashed password 
  sqlite3_open("database.db", &db); 

  // TODO error handling

  sql = "ALTER TABLE users ADD COLUMN password TEXT";
  sqlite3_prepare_v2(db, sql.c_str(), sql.size(), &stmt, NULL);
  sqlite3_step(stmt);
  sqlite3_reset(stmt);

  sqlite3_finalize(stmt);
  sqlite3_close(db);

  // Add a column which requires the user to change their password
  // This will be used as a way to send out password reset emails later on

  sqlite3_open("database.db", &db); 
  sql = "ALTER TABLE users ADD COLUMN reset_password BOOLEAN DEFAULT 0";
  sqlite3_prepare_v2(db, sql.c_str(), sql.size(), &stmt, NULL);
  sqlite3_step(stmt);
  sqlite3_reset(stmt);

  sqlite3_finalize(stmt);
  sqlite3_close(db);

  // Add one more user: Michael Friedrich, mfriedrich@gitlab.com
  sqlite3_open("database.db", &db);  // TODO remove after refactoring 

  User user3 = {3, "Michael Friedrich", "mfriedrich@gitlab.com"};

  if (insertUser(db, user3) == SQLITE_DONE) {
    std::cout << "User " << user3.name << "inserted successfully" << std::endl;
  } else {
    std::cout << "Error inserting user " << user3.name << std::endl;
  }

  // Print all users in the database table users
  sqlite3_open("database.db", &db);

  sql = "SELECT * FROM users";
  sqlite3_prepare_v2(db, sql.c_str(), sql.size(), &stmt, NULL);

  while (sqlite3_step(stmt) == SQLITE_ROW) {
    int id = sqlite3_column_int(stmt, 0);
    const unsigned char* name = sqlite3_column_text(stmt, 1);
    const unsigned char* email = sqlite3_column_text(stmt, 2);
    std::cout << id << " " << name << " " << email << std::endl;
  }

  sqlite3_finalize(stmt);
  sqlite3_close(db);


  return 0;
}






