#ifndef DB_H
#define DB_H

#include <sqlite3.h>
#include <string>

struct User {
  int id;
  std::string name;
  std::string email;
};

int createUserTable(sqlite3* db);
int insertUser(sqlite3* db, const User& user);
int printUsers(sqlite3* db);


#endif 